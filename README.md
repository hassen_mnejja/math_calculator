# Math Calculator

Math Calculator is a simple Python package that provides basic math calculations.

## Installation

You can install Math Calculator using pip:

```bash
pip install math_calculator
```

## Usage

```python
from math_calculator.calculator import add, subtract, multiply, divide

result = add(3, 5)
print(result)  # Output: 8

result = subtract(10, 4)
print(result)  # Output: 6

result = multiply(2, 3)
print(result)  # Output: 6

result = divide(10, 2)
print(result)  # Output: 5
```

## Contributing

Contributions are welcome! If you find any issues or have suggestions for improvements, please open an issue or submit a pull request.

## LICENSE

This project is licensed under the [MIT License](LICENSE). See the [LICENSE](LICENSE) file for more information.
